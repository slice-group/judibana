module Blog

	class << self
    mattr_accessor :site_name, :commentable, :votable, :comments_facebook, :widget_twitter_id
    # add default values of more config vars here
  end

	# this function maps the vars from your app into your engine
	def self.setup(&block)
	  yield self
	end

end