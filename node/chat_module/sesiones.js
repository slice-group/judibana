//                                             SESIONES.JS
//-------------------------------------------------------------------------------------------------------------

var arrayRemove = require('jqb-array-remove'); //remover elementos del array


module.exports.connect_admin = function (socket, data, admin) {
  if (!admin[data.id]) { //si el user.admin no existe
    socket.user = data; //se agregan los datos del user al socket
    admin[data.id] = []; //se asigna una key[] a la variable{} de session con el id del user.admin
    admin[data.id].push(socket); //se almacena el socket.id en la key[]
  } else { //si el user.admin ya existe
    socket.user = data; //se agregan los datos del user al socket
    admin[data.id].push(socket); //se almacena el socket.id en la key[]
  }
}

module.exports.list_clients = function(admin_id, clients, admin, io){
	var all_connect_clients = []; //se crea un array donde estaran todos los clientes que estan conectados
  for (var k in clients) { //recorrer el hash de clientes
    if (clients.hasOwnProperty(k)) {
      //console.log('key is: ' + k + ', value is: ' + clients[k][0].client);
      all_connect_clients.push(clients[k][0].client) //se inserta cad auno de los clientes conectados al array
    }
  }

  for(var i = 0; i<admin[admin_id].length; i++) { //se recorre el el array donde estan todos los sockets pertenecientes al admin que se le enviara la emisión
    io.sockets.socket(admin[admin_id][i].id).emit("show_all_client", all_connect_clients); // se envia la emisión con un array que contiene todos los clientes conectados.
  }
}

module.exports.hash_blank = function(hash) {
    var count = 0;
    for ( property in hash ) count++;
    return count;
}

module.exports.connect_client = function (socket, data, clients) {
  if (!clients[data.id]) { //si el user.client no existe
    socket.client = data; //se agregan los datos del user al socket
    clients[data.id] = []; //se asigna una key[] a la variable{} de session con el id del user.client
    clients[data.id].push(socket); //se almacena el socket.id en la key[]
  } else { //si el user.client ya existe
    socket.client = data; //se agregan los datos del user al socket
    clients[data.id].push(socket); //se almacena el socket.id en la key[]
  }
}

module.exports.disconnect_admin = function (socket, admin, clients, io) {
  //eliminar socket.id de la variable[] de session para administradores
  //si existe
	if(socket.user) {
	  admin[socket.user.id]=arrayRemove(admin[socket.user.id], socket);
    console.log(socket.user)
    if(clients[socket.user.chat_to]) {
      io.sockets.socket(clients[socket.user.chat_to][0].id).emit("close_chat_admin", { msg: "El Administrador a abandonado la conversación"});
	  }
  }

	//si no hay ningun socket conectado se eliminar user.admin de la variable de session admin{}
	if (socket.user && admin[socket.user.id].length == 0) {
	  delete admin[socket.user.id]
	};
}

module.exports.disconnect_client = function (socket, clients, io) {
  //eliminar socket.id de la variable[] de session para clientes
  //si existe
  if(socket.client) {
    io.sockets.emit("delete_index_chat", { id: socket.client.id } ) //emit socket para eliminar row de chat
    clients[socket.client.id]=arrayRemove(clients[socket.client.id], socket);
    io.sockets.socket(socket.client.chat_to).emit("close_chat_client", { msg: "El cliente a abandonado la conversación"});
  }
  
  //si no hay ningun socket conectado se eliminar user.client de la variable de session admin{}
  if (socket.client && clients[socket.client.id].length == 0) {
    delete clients[socket.client.id]
  };
}

module.exports.send_message = function(data, io, admin, clients, sck){
  if(clients[data.client_id] && admin[data.admin_id]) {
    assign_socket_to_chat(data, clients, sck) //asignar socket al que se le enviara el msg
    data_client = clients[data.client_id][0].client
    data_admin = admin[data.admin_id][0].user
    if (data.from == "admin") {
      io.sockets.socket(clients[data.client_id][0].id).emit("send-message-client", { client: data_client, admin: data_admin, msg: data.msg });
    } else if(data.from == "client") {
      if (admin[data.admin_id]) {
        io.sockets.socket(clients[data.client_id][0].client.chat_to).emit("send-message-admin", { client: data_client, admin: data_admin, msg: data.msg });
      }      
    }
  }
}

module.exports.send_all_admins = function(admin, io, channel, data) {
  for (var k in admin) { //recorrer el hash de admin
    if (admin.hasOwnProperty(k)) {  
      for(var i = 0; i<admin[k].length; i++) { //se recorre el el array donde estan todos los sockets pertenecientes al admin que se le enviara la emisión
        io.sockets.socket(admin[k][i].id).emit(channel, data);
      }
    }
  }
}

//Metodo para asignar el socket.admin especifico a client
function assign_socket_to_chat(data, clients, sck) {
  if (sck.user) {
    clients[data.client_id][0].client.chat_to = sck.id
    sck.user.chat_to = data.client_id
  } 
}

