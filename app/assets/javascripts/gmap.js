var map, map2;
var markersArray = [];
var latlng = new google.maps.LatLng(11.6876379,-70.2090108);
var latlng2 = new google.maps.LatLng(11.7576652,-70.1808752);

function initialize()
{  
  var myOptions = {
      zoom: 16,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false
  };

  var myOptions2 = {
      zoom: 16,
      center: latlng2,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false
  };

  map = new google.maps.Map(document.getElementById("map_canvas1"), myOptions);
  placeMarker(latlng, map, "C.C. Caribe, Punto Fijo, Falcón, Venezuela");

  map2 = new google.maps.Map(document.getElementById("map_canvas2"), myOptions2);
  placeMarker(latlng2, map2, "C.C. Jardin, Punto Fijo, Falcón, Venezuela");
}

function placeMarker(location, map, title) {    
	var marker = new google.maps.Marker({
	    position: location, 
	    map: map,
	    animation: google.maps.Animation.BOUNCE,
	    title: title,
	    icon: "/assets/favicon.png"
	});
	// add marker in markers array
	markersArray.push(marker);	
}


google.maps.event.addDomListener(window, 'load', initialize);
