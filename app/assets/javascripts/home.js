$( document ).ready(function() {
	$("#agencia_caribe").click(function(){
		desbloquear();
		$("#vendedor").html("");
		$("#vendedor").append("<option>Selecciona un vendedor</option>");
		$("#vendedor").append("<option>Bryam Weffer</option>");
		$("#vendedor").append("<option>Grace Ventura</option>");
		$("#vendedor").append("<option>Ruxbelis Goitia</option>");
	});	
	$("#agencia_jardin").click(function(){
		desbloquear();
		$("#vendedor").html("");
		$("#vendedor").append("<option>Selecciona un vendedor</option>");		
		$("#vendedor").append("<option>Mariangel Medina</option>");
		$("#vendedor").append("<option>Maritza Santander</option>");
		$("#vendedor").append("<option>Elycris Ramos</option>");
		$("#vendedor").append("<option>Maury Alvarado</option>");
		$("#vendedor").append("<option>Mery Falcon</option>");
	});

	function desbloquear(){
		$("#nombre").removeAttr("disabled");
		$("#nombre").css("background", "#fff");
		$("#telefono").removeAttr("disabled");
		$("#telefono").css("background", "#fff");
		$("#cedula").removeAttr("disabled");
		$("#cedula").css("background", "#fff");
		$("#email").removeAttr("disabled");
		$("#email").css("background", "#fff");
		$("#vendedor").removeAttr("disabled");
		$("#vendedor").css("background", "#fff");
		$("#adultos").removeAttr("disabled");
		$("#adultos").css("background", "#fff");
		$("#mayores").removeAttr("disabled");
		$("#mayores").css("background", "#fff");
		$("#nino").removeAttr("disabled");
		$("#nino").css("background", "#fff");
		$("#bebes").removeAttr("disabled");
		$("#bebes").css("background", "#fff");
		$("#id-btn-reserva").removeAttr("disabled");

		if($('#theme_ida').is(':checked')){
			$("#origen").removeAttr("disabled");
			$("#origen").css("background", "#fff");
			$("#salida").removeAttr("disabled");
			$("#salida").css("background", "#fff");
		}
		if($('#theme_ida_vuelta').is(':checked')){
			$("#origen").removeAttr("disabled");
			$("#origen").css("background", "#fff");
			$("#destino").removeAttr("disabled");
			$("#destino").css("background", "#fff");
			$("#salida").removeAttr("disabled");
			$("#salida").css("background", "#fff");
			$("#retorno").removeAttr("disabled");
			$("#retorno").css("background", "#fff");
		}
	}
	$('#theme_ida').change(function() {
		if($('#nombre').css('background-color') == "rgb(255, 255, 255)"){
			if($('#theme_ida').is(':checked')){
				$("#origen").removeAttr("disabled");
				$("#origen").css("background", "#fff");
				$("#salida").removeAttr("disabled");
				$("#salida").css("background", "#fff");
				$("#destino").attr("disabled");
				$("#destino").css("background", "rgb(235, 235, 235)");
				$("#retorno").attr("disabled");
				$("#retorno").css("background", "rgb(235, 235, 235)");
			}
		}
	});
	$('#theme_ida_vuelta').change(function() {
		if($('#nombre').css('background-color') == "rgb(255, 255, 255)"){
			if($('#theme_ida_vuelta').is(':checked')){
				$("#destino").removeAttr("disabled");
				$("#destino").css("background", "#fff");
				$("#retorno").removeAttr("disabled");
				$("#retorno").css("background", "#fff");
			}
		}
	});


});