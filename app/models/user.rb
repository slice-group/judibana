class User < ActiveRecord::Base
	before_create :add_role_default

	has_many :posts, dependent: :destroy
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  def add_role_default
    if User.blank?
      self.add_role "admin"
    else 
      self.add_role "escritor"
    end
  end


end


