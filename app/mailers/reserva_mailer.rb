class ReservaMailer < ActionMailer::Base
  default from: "principal@judibanaviajes.com"

  def reserva(client)
    @theme = client[:theme].capitalize
    @agencia = client[:agencia].capitalize
    @cedula = client[:cedula].capitalize
    @nombre = client[:nombre]
    @apellido = client[:apellido]
    @email = client[:email].capitalize
    @origen = client[:origen].capitalize
    @destino = client[:destino].capitalize
    @salida = client[:salida].capitalize
    @retorno = client[:retorno].capitalize
    @mayores = client[:mayores].capitalize if client[:mayores] != "Mayores" 
    @adultos = client[:adultos].capitalize if client[:adultos] != "Adultos" 
    @nino = client[:nino].capitalize if client[:nino] != "Niños" 
    @bebes = client[:bebes].capitalize if client[:bebes] != "Bébes"
    emails = {
        "Mariangel Medina"=>"ventas1@judibanaviajes.com",
        "Maritza Santander"=>"ventas2@judibanaviajes.com",
        "Elycris Ramos"=>"ventas3@judibanaviajes.com",
        "Maury Alvarado"=>"ventas4@judibanaviajes.com",
        "Mery Falcon"=>"ventas5@judibanaviajes.com",
        "Bryam Weffer"=>"ventas6@judibanaviajes.com",
        "Grace Ventura"=>"ventas7@judibanaviajes.com",
        "Ruxbelis Goitia"=>"ventas8@judibanaviajes.com",
    }

    mail(to: emails[client[:vendedor]], subject: @nombre+' ha contactado con Judibana Viajes.')

  end

  def reserva_paquete(client)
    @agencia = client[:agencia].capitalize
    @cedula = client[:cedula].capitalize
    @nombre = client[:nombre]
    @apellido = client[:apellido]
    @email = client[:email].capitalize
    @origen = client[:origen].capitalize
    @destino = client[:destino].capitalize
    @salida = client[:salida].capitalize
    @retorno = client[:retorno].capitalize
    @mayores = client[:mayores].capitalize if client[:mayores] != "Mayores"
    @adultos = client[:adultos].capitalize if client[:adultos] != "Adultos"
    @nino = client[:nino].capitalize if client[:nino] != "Niños"
    @bebes = client[:bebes].capitalize if client[:bebes] != "Bébes"
    emails = {
        "Mariangel Medina"=>"ventas1@judibanaviajes.com",
        "Maritza Santander"=>"ventas2@judibanaviajes.com",
        "Elycris Ramos"=>"ventas3@judibanaviajes.com",
        "Maury Alvarado"=>"ventas4@judibanaviajes.com",
        "Mery Falcon"=>"ventas5@judibanaviajes.com",
        "Bryam Weffer"=>"ventas6@judibanaviajes.com",
        "Grace Ventura"=>"ventas7@judibanaviajes.com",
        "Ruxbelis Goitia"=>"ventas8@judibanaviajes.com",
    }

    mail(to: emails[client[:vendedor]], subject: @nombre+' ha contactado con Judibana Viajes.')
  end
end
