class ContactMailer < ActionMailer::Base
  default from: "info@judibanaviajes.com"

  def contact(client)
    @name = client[:name]
    @email = client[:email]
    @comment = client[:comment]
    mail(to: "info@judibanaviajes.com", subject: @name+' ha contactado con Judibana Viajes.')
  end
end
