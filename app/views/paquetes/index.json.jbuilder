json.array!(@paquetes) do |paquete|
  json.extract! paquete, :id, :name, :description, :image, :price
  json.url paquete_url(paquete, format: :json)
end
