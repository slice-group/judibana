class HomeController < ApplicationController

	def index
		@paquetes = Paquete.order("RANDOM()").limit(4)
		@posts = Post.all.limit(3).order("created_at DESC")
	end

	def about		
	end

	def packages
		@paquetes = Paquete.all		
	end

	def show_package
		@paquete = Paquete.find(params[:id])		
	end

	def news
  	if params[:search]
			@posts = Post.search(params[:search])
			if @posts.class == Array
			  @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(3) 
			else
			  @posts = @posts.page(params[:page]).per(3)
			end
		else 
			 @posts = Post.all.order('id DESC')
			 @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(3) 
    end
    @recents_posts = Post.first(5).reverse
	end


	def contact_us
	end

	def action_complete
	end

	def show
		@post = Post.find(params[:id])
		@recents_posts = Post.first(5).reverse
	end

	def send_contact_message
	  if verify_recaptcha(attribute: "contact", message: "Oh! It's error with reCAPTCHA!")
	  	ContactMailer.contact(params).deliver
	  	redirect_to home_action_complete_path,  flash: { notice: params[:name]+', ¡Tu mensaje ha sido en enviado!' }	  	
	  else
	  	redirect_to home_contact_us_path, flash: { notice: '<div class="alert alert-danger" role="alert"><b>Error en los datos de envio: </b>'+params[:name]+', ¡Tu mensaje no ha sido en enviado! </div>' }
	  end
	end

	def search_affiliate
    unless params[:search].blank?
      @affiliate = Affiliate.search(params[:search]).first
      if @affiliate.blank?
        redirect_to new_affiliate_path, flash: { danger: 'No se encuenta registrado.' }
      elsif params[:rif_letter] == @affiliate.rif_type
        redirect_to affiliate_path(@affiliate.id)
      else
      	redirect_to new_affiliate_path, flash: { danger: 'No se encuenta registrado.' }
      end
    else
    	redirect_to new_affiliate_path, flash: { danger: 'No se encuenta registrado.' }
    end
  end

  def send_reservar
  	if params[:nombre].blank? and params[:apellido].blank? and params[:cedula].blank? and params[:email].blank?
  		flash[:msn] = "Su petición no fué enviada con exito debido aque los datos del formulario fueron ingresados incorrectamente."
  		redirect_to home_action_complete_path,  flash: { notice: params[:nombre]+', ¡Tu reserva no pudo se procesada!' }		
	else
		ReservaMailer.reserva(params).deliver
		flash[:msn] = "Su petición fué enviada con exito, pronto nos pondremos en contacto con usted."
	  	redirect_to home_action_complete_path,  flash: { notice: params[:nombre]+', ¡Tu reserva ha realizado exitosamente!' }
	end
  end

  def send_reservar_paquete
  	if params[:nombre].blank? and params[:apellido].blank? and params[:cedula].blank? and params[:email].blank?
  		flash[:msn] = "Su petición no fué enviada con exito debido aque los datos del formulario fueron ingresados incorrectamente."
  		redirect_to home_action_complete_path,  flash: { notice: params[:nombre]+', ¡Tu reserva no pudo se procesada!' }		
	else
		ReservaMailer.reserva_paquete(params).deliver
		flash[:msn] = "Su petición fué enviada con exito, pronto nos pondremos en contacto con usted."
	  	redirect_to home_action_complete_path,  flash: { notice: params[:nombre]+', ¡Tu reserva ha realizado exitosamente!' }
	end
  end

end
