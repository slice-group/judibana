class PaquetesController < ApplicationController
  before_action :set_paquete, only: [:show, :edit, :update, :destroy]

  # GET /paquetes
  # GET /paquetes.json
  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @paquetes = Paquete.all
  end

  # GET /paquetes/1
  # GET /paquetes/1.json
  def show
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
  end

  # GET /paquetes/new
  def new
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @paquete = Paquete.new
  end

  # GET /paquetes/1/edit
  def edit
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
  end

  # POST /paquetes
  # POST /paquetes.json
  def create
    @paquete = Paquete.new(paquete_params)

    respond_to do |format|
      if @paquete.save
        format.html { redirect_to @paquete, notice: 'Paquete was successfully created.' }
        format.json { render action: 'show', status: :created, location: @paquete }
      else
        format.html { render action: 'new' }
        format.json { render json: @paquete.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /paquetes/1
  # PATCH/PUT /paquetes/1.json
  def update
    respond_to do |format|
      if @paquete.update(paquete_params)
        format.html { redirect_to @paquete, notice: 'Paquete was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @paquete.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paquetes/1
  # DELETE /paquetes/1.json
  def destroy
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @paquete.destroy
    respond_to do |format|
      format.html { redirect_to paquetes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_paquete
      @paquete = Paquete.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def paquete_params
      params.require(:paquete).permit(:name, :description, :image, :price)
    end
end
