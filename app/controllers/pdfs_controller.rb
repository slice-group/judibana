class PdfsController < ApplicationController
	layout false

  def registration_sheet
  	@affiliate = Affiliate.find(params[:id])
  	respond_to do |format|
  		format.html # show.html.erb
      format.pdf { render :text => PDFKit.new( pdf_registration_sheet_path(@affiliate.id) ).to_pdf }
    end
  end

  def download
	  send_file(
    "#{Rails.root}/app/assets/pdfs/#{params[:document]}.pdf",
    filename: "#{params[:document]}",
    type: "application/pdf"
  	)
	end
end
