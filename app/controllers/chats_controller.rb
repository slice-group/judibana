class ChatsController < ApplicationController
	layout :resolve_layout
	def index
  end

  def client
  end

  def windows_chat
    @user = params
  end

  def admin_chat
  end

  private

  def resolve_layout
    case action_name
	    when "index"
	      "application"
	    else
	      "chat"
    end
  end
end
