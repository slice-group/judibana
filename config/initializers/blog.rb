require "#{Rails.root}/lib/engine.rb"

Blog.setup do |config|
	
  config.commentable = false #true = posts comentables | false = posts no commentables
  config.site_name = "Judibana Viajes" #Nombre del sitio web
  config.votable = true #true = posts votables | false = posts no votables
  config.comments_facebook = true #true = posts comentables  facebook| false = posts no commentables facebook
  config.widget_twitter_id = '498819909908824064' #add id widget timeline twitter
  
end
