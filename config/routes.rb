Blogadding::Application.routes.draw do
  
  resources :paquetes

  get "chats/index"
  get "chats/client"
  post "chats/windows_chat"
  get "chats/admin_chat/:id/:name", to: "chats#admin_chat", as: "chats_admin_chat"
  mount Ckeditor::Engine => '/ckeditor'
  get "home/action_complete", path: "done"
  root :to => "home#index"
  devise_for :users, :skip => [:registrations] 
  resources :users
  resources :admin, only: [:index]
  resources :blog, only: [:index, :show], as: 'blog', controller: 'blogadd'
  get "package/show/:id", to: "home#show_package", as: "show_package"
  get "home/about", path: "about"
  get "home/packages", path: "packages"
  get "home/news", path: "news"
  #get "home/links", path: "links"
  get "home/contact_us", path: "contact_us"
  get "home/news/:id/show",to: "home#show", as: "news_show"
  post "home/send_contact_message", path: "contact_send"
  post "home/send_reservar", path: "send_reservar"
  post "home/send_reservar_paquete", path: "send_reservar_paquete"
  #pdfs
  get "pdfs/registration_sheet/:id", to: "pdfs#registration_sheet", as:"pdf_registration_sheet"
  #get "pdfs/download/:document", to: "pdfs#download", as:"pdf_download"


  scope :admin do
  	resources :posts
  	resources :categories
  end

  match "/posts/add_new_comment" => "posts#add_new_comment", :as => "add_new_comment_to_posts", :via => [:post]
 
end