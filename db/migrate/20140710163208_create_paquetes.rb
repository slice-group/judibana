class CreatePaquetes < ActiveRecord::Migration
  def change
    create_table :paquetes do |t|
      t.string :name
      t.text :description
      t.string :image
      t.string :price

      t.timestamps
    end
  end
end
