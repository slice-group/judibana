# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :post do
    title ""
    content ""
    public ""
    category_id ""
    user_id ""
    image "MyString"
  end
end
